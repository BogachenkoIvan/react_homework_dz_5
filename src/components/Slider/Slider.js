import React from 'react';
import Slider from 'react-slick';
import './Slider.css';
import image1 from './img/image1.jpg';
import image2 from './img/image2.jpg';
import image3 from './img/image3.jpg';
import image4 from './img/image4.jpg';
import image5 from './img/image5.jpg';

function ProjectSlider() {
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
  };

  return (
    <div className='slider_block'>
      <h2>Мої проекти</h2>
      <Slider {...settings}>
        <div>
          <img src={image1} alt="Slide 1" />
        </div>
        <div>
          <img src={image2} alt="Slide 2" />
        </div>
        <div>
          <img src={image3} alt="Slide 3" />
        </div>
        <div>
          <img src={image4} alt="Slide 4" />
        </div>
        <div>
          <img src={image5} alt="Slide 5" />
        </div>
      </Slider>
    </div>
  );
}

export default ProjectSlider;



// import React from 'react';
// import Slider from 'react-slick';
// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css';
// import './Slider.css';

// function ProjectSlider() {
//   const images = [
//     { id: 1, src: "./img/image1.png", alt: "Slide 1" },
//     { id: 2, src: "./img/image2.png", alt: "Slide 2" },
//     { id: 3, src: "./img/image3.png", alt: "Slide 3" },
//     { id: 4, src: "./img/image4.png", alt: "Slide 4" },
//     { id: 5, src: "./img/image5.png", alt: "Slide 5" },
//   ];

//   const settings = {
//     dots: true,
//     infinite: true,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//     autoplay: true,
//     autoplaySpeed: 2000,
//   };

//   return (
//     <div className="slider_block">
//       <h2>Мої проекти</h2>
//       <Slider {...settings}>
//         {images.map((image) => (
//           <div key={image.id}>
//             <img src={image.src} alt={image.alt}/>
//           </div>
//         ))}
//       </Slider>
//     </div>
//   );
// }

// export default ProjectSlider;


