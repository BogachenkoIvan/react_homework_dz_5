import React from 'react';
import './Resume.css';
import certificatePhoto from './img/Certificate.jpg';

function Resume() {
  return (
    <div className="resume-container">
      <h2>Резюме</h2>
      <p>
          Front-end розробник з 5-місячним комерційним досвідом. Починав як розробник верстки для невеликої аутсорсингової компанії.<br></br>

          Я вивчаю React з початку 2023 року і хотів би приєднатися до проекту, щоб застосувати свої знання в комерційному середовищі.<br></br>
          Посилання на мої проекти (Gitlab): <a href="https://gitlab.com/BogachenkoIvan/">Gitlab</a><br></br>

          Комерційний досвід роботи з HTML/CSS<br></br>
          Знання JavaScript/React<br></br>
          Адаптивні/кросбраузерні/семантичні/еластичні методи компонування<br></br>
          Знання Git<br></br>
          Робота з Adobe Photoshop, Figma<br></br>
      </p>
      <div className="resume-certificate">
        <h3>Мої сертифікати:</h3>
        <img src={certificatePhoto} alt="Certicate" />;
      </div>
    </div>
  );
}

export default Resume;