import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Main from '../Main/Main';
import Resume from '../Resume/Resume';
import Projects from '../Projects/Projects';
import Photo from '../Photo/Photo';
import './App.css';

function App() {
  return (
    <Router>
      <div>
        <nav className='nav_menu'>
          <div className='nav_item'>
            <ul>
              <li>
                <Link to="/" className='nav_link'>Головна</Link>
              </li>
              <li>
                <Link to="/Resume" target="_blank" className='nav_link'>Резюме</Link>
              </li>
              <li>
                <Link to="/Projects" target="_blank" className='nav_link'>Мої проекти</Link>
              </li>
              <li>
                <Link to="https://www.linkedin.com/in/ivan-bogachenko-8148a9280/" target="_blank" className='nav_link'>LinkedIn</Link>
              </li>
            </ul>
          </div>          
          <Photo />
        </nav>
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/Resume" element={<Resume />} />
          <Route path="/Projects" element={<Projects />} />
        </Routes>        
      </div>
    </Router>
  );
}

export default App;