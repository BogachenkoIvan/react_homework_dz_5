import React from 'react';
import ProjectSlider from '../Slider/Slider';

function Projects() {
  return (
    <div>
      <ProjectSlider />
    </div>
  );
}

export default Projects;