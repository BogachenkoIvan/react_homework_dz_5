import React from 'react';
import Planet from '../Planet/Planet';

const Main = () => {
  return (
    <div className="main">
      <Planet />
    </div>
  );
};

export default Main;