import React from 'react';
import myPhoto from './img/my_photo.jpg';

function Photo() {
  return <div className="my_photo"><img  src={myPhoto} alt="My Photo" /></div>;
}

export default Photo;