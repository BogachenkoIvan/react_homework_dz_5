import React from 'react';
import './Planet.css'; 
import planetPhoto from './img/Planet.jpg';

const Planet = () => {
  return <div className="planet"><img src={planetPhoto} alt="Planet_Photo" /></div>;
};

export default Planet;